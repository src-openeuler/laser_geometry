# laser_geometry

#### Description
This package contains a class for converting from a 2D laser scan as defined by sensor_msgs/LaserScan into a point cloud as defined by sensor_msgs/PointCloud or sensor_msgs/PointCloud2. In particular, it contains functionality to account for the skew resulting from moving robots or tilting laser scanners.

#### Software Architecture
Software architecture description

The laser_geometry package contains a single C++ class: LaserProjection. There is no ROS API.
This class has two relevant functions for transforming from sensor_msgs/LaserScan to sensor_msgs/PointCloud or sensor_msgs/PointCloud2.

input:
```
laser_geometry/
├── CHANGELOG.rst
├── CMakeLists.txt
├── include
│   └── laser_geometry
├── LICENSE
├── package.xml
├── setup.py
├── src
│   ├── laser_geometry
│   └── laser_geometry.cpp
└── test
    ├── projection_test.cpp
    └── projection_test.py
```

#### Installation

1.  Download RPM

aarch64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_aarch64/aarch64/ros-noetic-ros-laser_geometry/ros-noetic-ros-laser_geometry-1.6.7-1.oe2203.aarch64.rpm

x86_64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_x86_64/x86_64/ros-noetic-ros-laser_geometry/ros-noetic-ros-laser_geometry-1.6.7-1.oe2203.x86_64.rpm 

2.  Install RPM

aarch64:

sudo rpm -ivh ros-noetic-ros-laser_geometry-1.6.7-1.oe2203.aarch64.rpm --nodeps --force

x86_64:

sudo rpm -ivh ros-noetic-ros-laser_geometry-1.6.7-1.oe2203.x86_64.rpm --nodeps --force

#### Instructions

Dependence installation

sh /opt/ros/noetic/install_dependence.sh

Exit the following output file under the /opt/ros/noetic/ directory , Prove that the software installation is successful

```
laser_geometry/
├── cmake.lock
├── env.sh
├── lib
│   ├── laser_geometry
│   ├── liblaser_geometry.so
│   ├── pkgconfig
│   └── python2.7
├── local_setup.bash
├── local_setup.sh
├── local_setup.zsh
├── setup.bash
├── setup.sh
├── _setup_util.py
├── setup.zsh
└── share
    └── laser_geometry
```


#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
