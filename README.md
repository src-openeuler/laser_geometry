# laser_geometry

#### 介绍
laser_geometry包含一个类，用于从sensor_msgs / LaserScan定义的2D激光扫描转换为sensor_msgs / PointCloud或sensor_msgs / PointCloud2定义的点云。特别是，它包含解决因移动机器人或倾斜激光扫描仪而导致的偏斜的功能。

#### 软件架构
软件架构说明

laser_geometry软件包包含一个C ++类：LaserProjection。没有ROS API。

此类具有两个相关功能，可用于从sensor_msgs / LaserScan转换为sensor_msgs / PointCloud或sensor_msgs / PointCloud2。

文件内容:
```
laser_geometry/
├── CHANGELOG.rst
├── CMakeLists.txt
├── include
│   └── laser_geometry
├── LICENSE
├── package.xml
├── setup.py
├── src
│   ├── laser_geometry
│   └── laser_geometry.cpp
└── test
    ├── projection_test.cpp
    └── projection_test.py
```

#### 安装教程

1. 下载rpm包

aarch64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_aarch64/aarch64/ros-noetic-ros-laser_geometry/ros-noetic-ros-laser_geometry-1.6.7-1.oe2203.aarch64.rpm

x86_64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_x86_64/x86_64/ros-noetic-ros-laser_geometry/ros-noetic-ros-laser_geometry-1.6.7-1.oe2203.x86_64.rpm  

2. 安装rpm包

aarch64:

sudo rpm -ivh ros-noetic-ros-laser_geometry-1.6.7-1.oe2203.aarch64.rpm --nodeps --force

x86_64:

sudo rpm -ivh ros-noetic-ros-laser_geometry-1.6.7-1.oe2203.x86_64.rpm --nodeps --force

#### 使用说明

依赖环境安装:

sh /opt/ros/noetic/install_dependence.sh

安装完成以后，在/opt/ros/noetic/目录下有如下输出,则表示安装成功

输出:
```
laser_geometry/
├── cmake.lock
├── env.sh
├── lib
│   ├── laser_geometry
│   ├── liblaser_geometry.so
│   ├── pkgconfig
│   └── python2.7
├── local_setup.bash
├── local_setup.sh
├── local_setup.zsh
├── setup.bash
├── setup.sh
├── _setup_util.py
├── setup.zsh
└── share
    └── laser_geometry
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
